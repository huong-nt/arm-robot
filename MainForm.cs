//----------------------------------------------------------------------------
//  Copyright (C) 2004-2013 by EMGU. All rights reserved.       
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Diagnostics;

namespace ShapeDetection
{
    public partial class MainForm : Form
    {
        private Capture _capture = null;
        private bool _captureInProgress;
        Image<Bgr, Byte> frame;
        public MainForm()
        {
            InitializeComponent();

            ShapeDetector.DisplayImage = triangleRectangleImageBox;
            try
            {
                _capture = new Capture(1);
                _capture.ImageGrabbed += ProcessFrame;
                _capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }
        private void ProcessFrame(object sender, EventArgs arg)
        {
            frame = _capture.RetrieveBgrFrame();
            ShapeDetector.PerformShapeDetection(frame);
            //originalImageBox.Image = frame;
        }
     
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void loadImageButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                fileNameTextBox.Text = openFileDialog1.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProcessFrame(this, null);
            ProcessFrame(this, null);
        }
    }
}
