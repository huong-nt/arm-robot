﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

using System.Reflection;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Threading;

namespace ShapeDetection
{
    public partial class Form1 : Form
    {
        #region Variables
        public float[] upper_bound = { 98.44f, 122.2f, 143f, 178.7f };
        public float[] lower_bound = { 146.2f, 105.2f, 143f, 178.7f };
        public float[] base_angles = { 149.7f, 148.2f, 149.7f, 198.9f };

        private SerialPort port = new SerialPort();

        private List<ServoMotor> motors;
        private RobotController mControl;

        private BufferedListView listView1;

        private bool Record = false;
        private bool Play = false;

        private List<double[]> angleRecord;
        
        //for object detection
        private Capture _capture = null;
        Image<Bgr, Byte> frame;
        private List<MCvBox2D> boxs;
        private PointF zero;
        #endregion

        public Form1()
        {
            InitializeComponent();

            listView1 = new BufferedListView();
            listView1.View = View.Details;

            listView1.Columns.Add("ID");
            listView1.Columns.Add("Angle");
            listView1.Columns.Add("Load");
            listView1.Columns.Add("Current Speed");
            listView1.Columns.Add("Speed limit");

            groupBox1.Controls.Add(listView1);

            listView1.Dock = DockStyle.Fill;

            listView1.GridLines = true;

            SetupCombobox();

            ServoMotor.port = this.port;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.Handshake = Handshake.None;
            port.DataBits = 8;

            motors = new List<ServoMotor>();

            //motors.Add(new ServoMotor(1));
            motors.Add(new ServoMotor(2));
            motors.Add(new ServoMotor(3));
            motors.Add(new ServoMotor(4));
            motors.Add(new ServoMotor(5));

            mControl = new RobotController(motors);

            //listView1.Items.Add(new ListViewItem(new string[5] { "1", "0", "0", "0", "0" }));
            listView1.Items.Add(new ListViewItem(new string[5] { "2", "0", "0", "0", "0" }));
            listView1.Items.Add(new ListViewItem(new string[5] { "3", "0", "0", "0", "0" }));
            listView1.Items.Add(new ListViewItem(new string[5] { "4", "0", "0", "0", "0" }));
            listView1.Items.Add(new ListViewItem(new string[5] { "5", "0", "0", "0", "0" }));

            
            //start camera 
            ShapeDetector.DisplayImage = triangleRectangleImageBox;
            try
            {
                _capture = new Capture(1);
                _capture.ImageGrabbed += ProcessFrame;
                _capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
            zero = new PointF(411, 226);


            timer1.Start();
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            frame = _capture.RetrieveBgrFrame();
            boxs = ShapeDetector.PerformShapeDetection(frame);

            
        }

        ~Form1()
        {
            if (port.IsOpen)
                port.Close();
        }

        private void SetupCombobox()
        {
            foreach (var c in SerialPort.GetPortNames())
            {
                toolStripComboBox1.Items.Add(c);
            }

            if (toolStripComboBox1.Items.Count != 0)
                toolStripComboBox1.SelectedIndex = 0;
        }


        private void OpenPort()
        {
            try
            {
                port.PortName = toolStripComboBox1.SelectedItem as string;
                port.BaudRate = int.Parse(toolStripTextBox1.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }

            port.Open();
            while (port.IsOpen == false) ;
            toolStripButton1.Text = "Close port(&C)";
            toolStripComboBox1.Enabled = false;
        }

        private void ClosePort()
        {
            port.Close();
            while (port.IsOpen == true) ;
            toolStripButton1.Text = "Open port(&C)";
            toolStripComboBox1.Enabled = true;
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (port.IsOpen == false)
            {
                OpenPort();
            }
            else
            {
                ClosePort();
            }
        }

        void readData()
        {
            int cnt = 0;

            //frame = _capture.RetrieveBgrFrame();
            //ShapeDetector.PerformShapeDetection(frame);

            string logAngles = "";

            foreach (var m in motors)
            {
                m.UpdateMotor();
                listView1.Items[cnt].SubItems[1].Text = m.SenseAngle.ToString("G4");
                listView1.Items[cnt].SubItems[2].Text = m.SenseLoad.ToString("G4");
                listView1.Items[cnt].SubItems[3].Text = m.SenseSpeed.ToString("G4");
                listView1.Items[cnt].SubItems[4].Text = m.SpeedLimit.ToString("G4");

                logAngles += m.SenseAngle.ToString("G4") + ", ";

                if (!m.TorqueON)
                {
                    m.TargetAngle = m.SenseAngle;
                }

                if (Record)
                    AddAngleRecord();

                if (Play)
                    SetAngleFromRecord();

                cnt++;
            }
            txtLog.Text = logAngles;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            readData();
        }

        private void btnTorqueOn_Click(object sender, EventArgs e)
        {
            TorqueOn();
        }

        private void btnTorqueOff_Click(object sender, EventArgs e)
        {
            TorqueOff();
        }

        private void TorqueOn()
        {
            foreach (var m in motors)
            {
                m.TorqueON = true;
            }
        }


        private void TorqueOff()
        {
            foreach (var m in motors)
            {
                m.TorqueON = false;
            }
        }


        //Record button
        private void btnRecord_Click(object sender, EventArgs e)
        {
            if (Record)
            {
                FinishRecord();
            }
            else
            {
                StartRecord();
            }
        }


        //Play button
        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (Play)
            {
                FinishPlay();
            }
            else
            {
                StartPlay();
            }
        }


        //Start button
        private void btnStart_Click(object sender, EventArgs e)
        {
            SetStartPosition();
        }


        private void SetStartPosition()
        {
            TorqueOn();
        }


        private void AddAngleRecord()
        {
            double[] angles = new double[5];

            //int i = 0;
            int i = 1;

            foreach (var m in motors)
            {
                angles[i] = m.SenseAngle;
                i++;
            }

            this.Text = angleRecord.Count.ToString();

            angleRecord.Add(angles);
        }


        int readIndex = 0;

        private void SetAngleFromRecord()
        {
            if (angleRecord.Count > (readIndex + 1))
            {
                //int i = 0;
                int i = 1;

                foreach (var m in motors)
                {
                    m.TargetAngle = angleRecord[readIndex][i];
                    i++;
                }
                readIndex++;
                this.Text = readIndex + "/" + angleRecord.Count.ToString();
            }
            else
            {
                FinishPlay();
            }
        }


        private void StartRecord()
        {
            btnRecord.Text = "StopRecord(&R)";
            btnPlay.Enabled = false;
            Record = true;

            angleRecord = new List<double[]>();

            TorqueOff();

        }

        private void FinishRecord()
        {
            btnRecord.Text = "Record(&R)";
            btnPlay.Enabled = true;
            Record = false;
            TorqueOn();
        }

        private void StartPlay()
        {
            btnPlay.Text = "StopPlay(&P)";
            btnRecord.Enabled = false;
            Play = true;
            readIndex = 0;
            TorqueOn();
            SetStartPosition();
        }

        private void FinishPlay()
        {
            btnPlay.Text = "Play(&P)";
            btnRecord.Enabled = true;
            Play = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboId.SelectedIndex = 0;
        }

        private void btnIncrease_Click(object sender, EventArgs e)
        {
            int id = comboId.SelectedIndex;
            int offset = Convert.ToInt16(txtAngleAmount.Text);
            mControl.changeAngle(id, offset);
        }

        private void btnDecrease_Click(object sender, EventArgs e)
        {
            int id = comboId.SelectedIndex;
            int offset = Convert.ToInt16(txtAngleAmount.Text);
            mControl.changeAngle(id, -offset);
        }

        private void txtSpeedLimit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int id = comboId.SelectedIndex;
                int speed = Convert.ToInt16(txtSpeedLimit.Text);
                mControl.setSpeed(id, speed);
            }
        }

        private void btnToUpper_Click(object sender, EventArgs e)
        {
            mControl.moveToBase(upper_bound);
        }

        private void btnToLower_Click(object sender, EventArgs e)
        {
            mControl.moveToBase(lower_bound);
        }

        private void syncData()
        {
            int cnt = 0;
            foreach (var m in motors)
            {
                m.UpdateMotor();

                listView1.Items[cnt].SubItems[1].Text = m.SenseAngle.ToString("G4");
                listView1.Items[cnt].SubItems[2].Text = m.SenseLoad.ToString("G4");
                listView1.Items[cnt].SubItems[3].Text = m.SenseSpeed.ToString("G4");
                listView1.Items[cnt].SubItems[4].Text = m.SpeedLimit.ToString("G4");
                cnt++;
            }
        }

        private void btnClick_Click(object sender, EventArgs e)
        {
            timer1.Stop();

            //mControl.performClick();
            mControl.fingerDown();

            syncData();

            System.Threading.Thread.Sleep(300);
            mControl.fingerUp();

            syncData();

            timer1.Start();
        }

        private void btnButton1_Click(object sender, EventArgs e)
        {
            MCvBox2D max;
            try
            {
                max = boxs[0];
            }
            catch (Exception ex)
            {
                return;
            }
            foreach(MCvBox2D box in boxs) {
                if (box.center.Y < max.center.Y) max = box;
            }
            PointF coord = convertCoordinate(max.center);
            float[] result = mControl.calcAngles((int)coord.X, (int)coord.Y);
            mControl.toAngle(0, base_angles[0] - result[0]);
            mControl.toAngle(1, base_angles[1] + result[1]);

            //float[] btn1 = { 131.3f, 112.5f, 165.8f, 195.4f };
            //timer1.Stop();
            //mControl.moveToBase(btn1);
            //syncData();
            //Thread.Sleep(500);
            ////btnClick_Click(null, null);
            //btnClick_Click(null, null);
            //syncData();
            //Thread.Sleep(500);
            //btnToUpper_Click(null, null);
            //syncData();
            //timer1.Start();
            
        }

        private void btnButton2_Click(object sender, EventArgs e)
        {
            float[] btn1 = { 120.1f, 111.9f, 142.4f, 195.4f };
            timer1.Stop();
            mControl.moveToBase(btn1);
            syncData();
            Thread.Sleep(500);
            //btnClick_Click(null, null);
            btnClick_Click(null, null);
            syncData();
            Thread.Sleep(500);
            btnToUpper_Click(null, null);
            syncData();
            timer1.Start();
        }

        private void btnButton3_Click(object sender, EventArgs e)
        {
            float[] btn1 = { 115.4f, 113.4f, 142.4f, 195.4f };
            timer1.Stop();
            mControl.moveToBase(btn1);
            syncData();
            Thread.Sleep(500);
            //btnClick_Click(null, null);
            btnClick_Click(null, null);
            syncData();
            Thread.Sleep(500);
            btnToUpper_Click(null, null);
            syncData();
            timer1.Start();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            float[] btn1 = { 130.7f, 112.2f, 160f, 195.4f };
            timer1.Stop();
            mControl.moveToBase(btn1);
            syncData();
            Thread.Sleep(500);
            btnClick_Click(null, null);
            syncData();
            Thread.Sleep(300);
            btnToUpper_Click(null, null);
            syncData();
            timer1.Start();
        }

        private void btnCali_Click(object sender, EventArgs e)
        {
            mControl.moveToBase(base_angles);
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            float[] result = mControl.calcAngles(Convert.ToInt16(txtX.Text), Convert.ToInt16(txtY.Text));
            lblResult.Text = result[0].ToString() + " " + result[1].ToString();
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            float[] result = mControl.calcAngles(Convert.ToInt16(txtX.Text), Convert.ToInt16(txtY.Text));
            mControl.toAngle(0, base_angles[0]-result[0]);
            mControl.toAngle(1, base_angles[1]+result[1]);
        }

        private PointF convertCoordinate(PointF p)
        {
            float dx, dy;
            int s;
            dx = (p.X - 419);
            s = (int)(dx / Math.Abs(dx));
            float absDx = Math.Abs(dx);
            dy = -(p.Y - 279)*10/20;
            if (Math.Abs(dx) <= 45) dx = absDx / 22;
            else if (Math.Abs(dx) <= 130) dx = (absDx - 2) / 21;
            else if (Math.Abs(dx) <= 225) dx = (absDx - 6 - 8 - 1) / 19;
            else dx = (Math.Abs(dx) - 8 - 12 - 2 - 4) / 18;
            return new PointF(275 + dx*10*s, dy);
        }
    }

    class BufferedListView : System.Windows.Forms.ListView
    {
        protected override bool DoubleBuffered { get { return true; } set { } }
    }

}
