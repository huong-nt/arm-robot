﻿using Emgu.CV;
namespace ShapeDetection
{
    partial class Form1
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTorqueOn = new System.Windows.Forms.Button();
            this.btnTorqueOff = new System.Windows.Forms.Button();
            this.btnRecord = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnStartPos = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSpeedLimit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAngleAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboId = new System.Windows.Forms.ComboBox();
            this.btnDecrease = new System.Windows.Forms.Button();
            this.btnIncrease = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnButton3 = new System.Windows.Forms.Button();
            this.btnButton1 = new System.Windows.Forms.Button();
            this.btnButton2 = new System.Windows.Forms.Button();
            this.btnCali = new System.Windows.Forms.Button();
            this.btnClick = new System.Windows.Forms.Button();
            this.btnToLower = new System.Windows.Forms.Button();
            this.btnToUpper = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.triangleRectangleImageBox = new Emgu.CV.UI.ImageBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.txtX = new System.Windows.Forms.TextBox();
            this.txtY = new System.Windows.Forms.TextBox();
            this.btnCalc = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnMove = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.triangleRectangleImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.toolStripTextBox1,
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1059, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel1.Text = "Baud:";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(60, 25);
            this.toolStripTextBox1.Text = "1000000";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(34, 22);
            this.toolStripLabel2.Text = "[bps]";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "Connect(&C)";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 154);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Motor status";
            // 
            // btnTorqueOn
            // 
            this.btnTorqueOn.Location = new System.Drawing.Point(335, 26);
            this.btnTorqueOn.Name = "btnTorqueOn";
            this.btnTorqueOn.Size = new System.Drawing.Size(263, 25);
            this.btnTorqueOn.TabIndex = 5;
            this.btnTorqueOn.Text = "TorqueON";
            this.btnTorqueOn.UseVisualStyleBackColor = true;
            this.btnTorqueOn.Click += new System.EventHandler(this.btnTorqueOn_Click);
            // 
            // btnTorqueOff
            // 
            this.btnTorqueOff.Location = new System.Drawing.Point(334, 58);
            this.btnTorqueOff.Name = "btnTorqueOff";
            this.btnTorqueOff.Size = new System.Drawing.Size(264, 25);
            this.btnTorqueOff.TabIndex = 6;
            this.btnTorqueOff.Text = "TorqueOFF";
            this.btnTorqueOff.UseVisualStyleBackColor = true;
            this.btnTorqueOff.Click += new System.EventHandler(this.btnTorqueOff_Click);
            // 
            // btnRecord
            // 
            this.btnRecord.Location = new System.Drawing.Point(335, 90);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(263, 25);
            this.btnRecord.TabIndex = 7;
            this.btnRecord.Text = "Record(&R)";
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(335, 154);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(263, 25);
            this.btnPlay.TabIndex = 8;
            this.btnPlay.Text = "Play(&P)";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnStartPos
            // 
            this.btnStartPos.Location = new System.Drawing.Point(335, 123);
            this.btnStartPos.Name = "btnStartPos";
            this.btnStartPos.Size = new System.Drawing.Size(263, 25);
            this.btnStartPos.TabIndex = 9;
            this.btnStartPos.Text = "StartPositon(&S)";
            this.btnStartPos.UseVisualStyleBackColor = true;
            this.btnStartPos.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSpeedLimit);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtAngleAmount);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.comboId);
            this.groupBox2.Controls.Add(this.btnDecrease);
            this.groupBox2.Controls.Add(this.btnIncrease);
            this.groupBox2.Location = new System.Drawing.Point(335, 186);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 136);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Moving controller";
            // 
            // txtSpeedLimit
            // 
            this.txtSpeedLimit.Location = new System.Drawing.Point(86, 107);
            this.txtSpeedLimit.Name = "txtSpeedLimit";
            this.txtSpeedLimit.Size = new System.Drawing.Size(160, 20);
            this.txtSpeedLimit.TabIndex = 7;
            this.txtSpeedLimit.Text = "100";
            this.txtSpeedLimit.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtSpeedLimit_PreviewKeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Speed limit";
            // 
            // txtAngleAmount
            // 
            this.txtAngleAmount.Location = new System.Drawing.Point(85, 79);
            this.txtAngleAmount.Name = "txtAngleAmount";
            this.txtAngleAmount.Size = new System.Drawing.Size(160, 20);
            this.txtAngleAmount.TabIndex = 5;
            this.txtAngleAmount.Text = "5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Angle amount";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select item ID";
            // 
            // comboId
            // 
            this.comboId.FormattingEnabled = true;
            this.comboId.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5"});
            this.comboId.Location = new System.Drawing.Point(85, 49);
            this.comboId.Name = "comboId";
            this.comboId.Size = new System.Drawing.Size(161, 21);
            this.comboId.TabIndex = 2;
            // 
            // btnDecrease
            // 
            this.btnDecrease.Location = new System.Drawing.Point(141, 20);
            this.btnDecrease.Name = "btnDecrease";
            this.btnDecrease.Size = new System.Drawing.Size(105, 23);
            this.btnDecrease.TabIndex = 1;
            this.btnDecrease.Text = "Decrease angle";
            this.btnDecrease.UseVisualStyleBackColor = true;
            this.btnDecrease.Click += new System.EventHandler(this.btnDecrease_Click);
            // 
            // btnIncrease
            // 
            this.btnIncrease.Location = new System.Drawing.Point(7, 20);
            this.btnIncrease.Name = "btnIncrease";
            this.btnIncrease.Size = new System.Drawing.Size(105, 23);
            this.btnIncrease.TabIndex = 0;
            this.btnIncrease.Text = "Increase angle";
            this.btnIncrease.UseVisualStyleBackColor = true;
            this.btnIncrease.Click += new System.EventHandler(this.btnIncrease_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnBack);
            this.groupBox3.Controls.Add(this.btnButton3);
            this.groupBox3.Controls.Add(this.btnButton1);
            this.groupBox3.Controls.Add(this.btnButton2);
            this.groupBox3.Controls.Add(this.btnCali);
            this.groupBox3.Controls.Add(this.btnClick);
            this.groupBox3.Controls.Add(this.btnToLower);
            this.groupBox3.Controls.Add(this.btnToUpper);
            this.groupBox3.Location = new System.Drawing.Point(0, 186);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(324, 136);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Functions";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(243, 99);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Button Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnButton3
            // 
            this.btnButton3.Location = new System.Drawing.Point(125, 99);
            this.btnButton3.Name = "btnButton3";
            this.btnButton3.Size = new System.Drawing.Size(75, 23);
            this.btnButton3.TabIndex = 5;
            this.btnButton3.Text = "Buttton 3";
            this.btnButton3.UseVisualStyleBackColor = true;
            this.btnButton3.Click += new System.EventHandler(this.btnButton3_Click);
            // 
            // btnButton1
            // 
            this.btnButton1.Location = new System.Drawing.Point(125, 19);
            this.btnButton1.Name = "btnButton1";
            this.btnButton1.Size = new System.Drawing.Size(75, 23);
            this.btnButton1.TabIndex = 4;
            this.btnButton1.Text = "Button 1";
            this.btnButton1.UseVisualStyleBackColor = true;
            this.btnButton1.Click += new System.EventHandler(this.btnButton1_Click);
            // 
            // btnButton2
            // 
            this.btnButton2.Location = new System.Drawing.Point(125, 61);
            this.btnButton2.Name = "btnButton2";
            this.btnButton2.Size = new System.Drawing.Size(75, 23);
            this.btnButton2.TabIndex = 3;
            this.btnButton2.Text = "Button 2";
            this.btnButton2.UseVisualStyleBackColor = true;
            this.btnButton2.Click += new System.EventHandler(this.btnButton2_Click);
            // 
            // btnCali
            // 
            this.btnCali.Location = new System.Drawing.Point(243, 20);
            this.btnCali.Name = "btnCali";
            this.btnCali.Size = new System.Drawing.Size(75, 23);
            this.btnCali.TabIndex = 7;
            this.btnCali.Text = "Calibrate";
            this.btnCali.UseVisualStyleBackColor = true;
            this.btnCali.Click += new System.EventHandler(this.btnCali_Click);
            // 
            // btnClick
            // 
            this.btnClick.Location = new System.Drawing.Point(13, 99);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(75, 23);
            this.btnClick.TabIndex = 2;
            this.btnClick.Text = "Click";
            this.btnClick.UseVisualStyleBackColor = true;
            this.btnClick.Click += new System.EventHandler(this.btnClick_Click);
            // 
            // btnToLower
            // 
            this.btnToLower.Location = new System.Drawing.Point(13, 61);
            this.btnToLower.Name = "btnToLower";
            this.btnToLower.Size = new System.Drawing.Size(75, 23);
            this.btnToLower.TabIndex = 1;
            this.btnToLower.Text = "To lower";
            this.btnToLower.UseVisualStyleBackColor = true;
            this.btnToLower.Click += new System.EventHandler(this.btnToLower_Click);
            // 
            // btnToUpper
            // 
            this.btnToUpper.Location = new System.Drawing.Point(13, 20);
            this.btnToUpper.Name = "btnToUpper";
            this.btnToUpper.Size = new System.Drawing.Size(75, 23);
            this.btnToUpper.TabIndex = 0;
            this.btnToUpper.Text = "To upper";
            this.btnToUpper.UseVisualStyleBackColor = true;
            this.btnToUpper.Click += new System.EventHandler(this.btnToUpper_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.triangleRectangleImageBox);
            this.groupBox4.Location = new System.Drawing.Point(604, 25);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(455, 297);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // triangleRectangleImageBox
            // 
            this.triangleRectangleImageBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.triangleRectangleImageBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.triangleRectangleImageBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.triangleRectangleImageBox.Location = new System.Drawing.Point(3, 16);
            this.triangleRectangleImageBox.Name = "triangleRectangleImageBox";
            this.triangleRectangleImageBox.Size = new System.Drawing.Size(449, 278);
            this.triangleRectangleImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.triangleRectangleImageBox.TabIndex = 5;
            this.triangleRectangleImageBox.TabStop = false;
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(13, 329);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(585, 20);
            this.txtLog.TabIndex = 13;
            this.txtLog.Text = "logs";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(13, 366);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(75, 20);
            this.txtX.TabIndex = 14;
            this.txtX.Text = "235";
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(131, 367);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(75, 20);
            this.txtY.TabIndex = 15;
            this.txtY.Text = "10";
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(233, 367);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(75, 23);
            this.btnCalc.TabIndex = 16;
            this.btnCalc.Text = "Calculate";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(332, 370);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(32, 13);
            this.lblResult.TabIndex = 17;
            this.lblResult.Text = "result";
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(523, 364);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(75, 23);
            this.btnMove.TabIndex = 18;
            this.btnMove.Text = "Move";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 423);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.txtY);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnStartPos);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnRecord);
            this.Controls.Add(this.btnTorqueOff);
            this.Controls.Add(this.btnTorqueOn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Arm robot control";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.triangleRectangleImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTorqueOn;
        private System.Windows.Forms.Button btnTorqueOff;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnStartPos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDecrease;
        private System.Windows.Forms.Button btnIncrease;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboId;
        private System.Windows.Forms.TextBox txtAngleAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSpeedLimit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnToLower;
        private System.Windows.Forms.Button btnToUpper;
        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.GroupBox groupBox4;
        private Emgu.CV.UI.ImageBox triangleRectangleImageBox;
        private System.Windows.Forms.Button btnCali;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnButton3;
        private System.Windows.Forms.Button btnButton1;
        private System.Windows.Forms.Button btnButton2;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnMove;
    }
}

