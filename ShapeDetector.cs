﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeDetection
{
    public class ShapeDetector
    {
        public static ImageBox DisplayImage;
        public static List<MCvBox2D> PerformShapeDetection(Image<Bgr, Byte> frame)
        {
            //Convert the image to grayscale and filter out the noise
            Image<Bgr, Byte> img = frame.Resize(640, 480, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            //Image<Bgr, byte> cpimg = img.Resize(width, height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            Image<Gray, Byte> gray = img.Convert<Gray, Byte>().PyrDown().PyrUp();

            double cannyThreshold = 180.0;
            double cannyThresholdLinking = 120.0;
            Image<Gray, Byte> cannyEdges = gray.Canny(cannyThreshold, cannyThresholdLinking);
            LineSegment2D[] lines = cannyEdges.HoughLinesBinary(
                    1,                  //Distance resolution in pixel-related units
                    Math.PI / 45.0,     //Angle resolution measured in radians.
                    20,                 //threshold
                    60,                 //min Line width
                    10                  //gap between lines
                )[0];                   //Get the lines from the first channel

            List<Triangle2DF> triangleList = new List<Triangle2DF>();
            List<MCvBox2D> boxList = new List<MCvBox2D>(); //a box is a rotated rectangle
            using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
                for (Contour<Point> contours = cannyEdges.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                                                                       Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST,
                                                                       storage);
                   contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                    if (currentContour.Area > 550 && currentContour.Area < 4000) //only consider contours with area greater than 250
                    {
                        if (currentContour.Total == 4) //The contour has 4 vertices.
                        {
                            bool isRectangle = true;
                            Point[] pts = currentContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                            for (int i = 0; i < edges.Length; i++)
                            {
                                double angle = Math.Abs(
                                   edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                                if (angle < 80 || angle > 100)
                                {
                                    isRectangle = false;
                                    break;
                                }
                            }
                            if (isRectangle)
                            {
                                boxList.Add(currentContour.GetMinAreaRect());
                            }
                            for (int i = 0; i < boxList.Count; i++)
                            {
                                int x = (int)boxList[i].center.X;
                                int y = (int)boxList[i].center.Y;
                                for (int j = i + 1; j < boxList.Count; j++)
                                {
                                    int x_t = (int)boxList[j].center.X;
                                    int y_t = (int)boxList[j].center.Y;
                                    int distanceThres = 4;
                                    if (Math.Abs(x - x_t) <= distanceThres && Math.Abs(y - y_t) <= distanceThres)
                                    {
                                        boxList.RemoveAt(j);
                                    }

                                }

                            }

                        }
                    }
                }

            //originalImageBox.Image = img;

            Image<Bgr, Byte> processedImage = img;
            foreach (MCvBox2D box in boxList)
                processedImage.Draw(box, new Bgr(Color.DarkOrange), 2);
            DisplayImage.Image = processedImage;
            return boxList;
        }

    }
}
