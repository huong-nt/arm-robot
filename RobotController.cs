﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeDetection
{
    class RobotController
    {
        const int A1 = 85;
        const int A2 = 190;

        private List<ServoMotor> motors; //group of motor object

        public RobotController(List<ServoMotor> pPotors)
        {
            this.motors = pPotors;
        }

        //Control basic action of item
        public void toAngle(int id, float angle)
        {
            if (id < 0 || id >= motors.Count) return;
            motors[id].TargetAngle = angle;
        }

        public void changeAngle(int id, float offset)
        {
            float destAngle = (float)Math.Ceiling(motors[id].SenseAngle) + offset;
            toAngle(id, destAngle);
        }

        public void setSpeed(int id, int pSpeed)
        {
            if (id < 0 || id >= motors.Count) return;
            if (pSpeed <= 0) return;
            motors[id].SpeedLimit = pSpeed;
        }

        //More complex actions
        public void moveToBase(float[] angles)
        {
            int i = 0;
            foreach (var m in motors)
            {
                //TODO: check input array
                m.TargetAngle = angles[i];
                i++;
            }
        }

        public void fingerUp()
        {
            motors[3].TargetAngle = 196f;
        }

        public void fingerDown()
        {
            motors[3].TargetAngle = 202f;
        }

        public float[] calcAngles(int x, int y)
        {
            float cos2 = (float)(x * x + y * y - A1 * A1 - A2 * A2) / (2 * A1 * A2);
            float p2 = (float)Math.Acos(cos2);
            p2 = p2 / (float)Math.PI * 180;

            float sin2 = (float)Math.Sqrt(1-cos2*cos2);
            //float cos1 = (x * (A1 + A2 * cos2) + y * A2 * sin2) / (x * x + y * y);
            //if (Math.Abs(cos1) > 1)
            //    cos1 = (x * (A1 + A2 * cos2) - y * A2 * sin2) / (x * x + y * y);
            //float p1 = (float)Math.Acos(cos1);
            float triAdjacent = A1 + A2 * cos2;
            float triOpposite = A2 * sin2;
            float tanY = y * triAdjacent - x * triOpposite;
            float tanX = x * triAdjacent + y * triOpposite;

            float p1 = (float)Math.Atan2(tanY, tanX);
            p1 = p1 / (float)Math.PI * 180;

            float[] result = {p1, p2};
            return result;
        }
    }
}
