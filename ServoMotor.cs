﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;

namespace ShapeDetection
{

    class RobotisPackethandler
    {
        private byte id = 1;
        private SerialPort port;
        readonly byte READ = 2;
        readonly byte WRITE = 3;

        public RobotisPackethandler(int ID, SerialPort port)
        {
            this.id = (byte)(ID & 0xff);
            this.port = port;
        }


        private byte calc_checksum_robotis(List<byte> packet)
        {
            int checksum = 0;

            for (int i = 2; i < packet.Count; i++)
            {
                checksum += (int)packet[i];
            }

            checksum = (~checksum) & 0xFF;

            return (byte)(checksum & 0xFF);
        }

        private void makeHeader(List<byte> packet, byte dataLength)
        {
            packet.Add(0xff);
            packet.Add(0xff);
            packet.Add(id);
            packet.Add((byte)(dataLength + 2));
        }

        public IEnumerable<byte> GetParameter(byte Address, byte Length)
        {
            List<byte> packet = new List<byte>();

            makeHeader(packet, 2);
            packet.Add(READ);
            packet.Add(Address);
            packet.Add(Length);
            packet.Add(calc_checksum_robotis(packet));

            if (port.IsOpen)
            {
                port.Write(packet.ToArray<byte>(), 0, packet.Count);
                return getStatusPacket(6 + Length);
            }

            return null;
        }

        public void SetParameter(byte Address, IEnumerable<byte> data)
        {
            List<byte> packet = new List<byte>();

            makeHeader(packet, (byte)(data.Count() + 1));

            packet.Add(WRITE);

            packet.Add(Address);

            foreach (var b in data)
            {
                packet.Add(b);
            }

            packet.Add(calc_checksum_robotis(packet));

            if (port.IsOpen)
            {
                port.Write(packet.ToArray<byte>(), 0, packet.Count);
                getStatusPacket(6);
            }
        }

        private IEnumerable<byte> getStatusPacket(int packetLength)
        {
            byte[] buff = new byte[packetLength];

            try
            {
                while (port.BytesToRead < packetLength) ;
                port.Read(buff, 0, packetLength);
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show(e.Message);
            }

            return buff;
        }





    }

    class ServoMotor
    {
        public static SerialPort port;

        private RobotisPackethandler packetHandler;

        public int ID { get; private set; }             //サーボID
        public bool TorqueON { get; set; }              //トルクON/OFF
        public double TargetAngle { get; set; }         //目標角度
        public double SenseAngle { get; private set; }  //読み取り角度
        public double SenseLoad { get; private set; }   //読み取り負荷
        public double SenseSpeed { get; private set; }  //読み取り速度
        public double SpeedLimit { get; set; }          //速度リミッタ

        public string message;

        public ServoMotor(int id)
        {
            if (port == null)
                System.Windows.Forms.MessageBox.Show("シリアルポートが設定される前にインスタンスが生成されました。");

            this.ID = id;

            packetHandler = new RobotisPackethandler(id, port);

            TorqueON = false;
            TargetAngle = 150;
            SpeedLimit = 20;

        }

        public void UpdateMotor()
        {
            if (port.IsOpen)
            {
                getServoStatus();
                setServoStatus();
            }
        }

        private void getServoStatus()
        {
            int offset = 5;
            List<byte> buff = packetHandler.GetParameter(0x24, 6).ToList();

            message = "";

            foreach (var b in buff)
            {
                message = message + b.ToString("X2");
                message = message + " ";
            }

            SenseAngle = calcAngleFromData(buff[offset + 0] + (buff[offset + 1] << 8));
            SenseSpeed = buff[offset + 2] + (buff[offset + 3] << 8); //TODO: may contain problem
            SenseLoad = calcLoadFromData(buff[offset + 4] + (buff[offset + 5] << 8));

        }

        private void setServoStatus()
        {

            packetHandler.SetParameter(0x18,
                new byte[] 
                {
                    0,
                    1,
                    getComplianceMargin(),
                    getComplianceMargin(),
                    getComplianceSlope(),
                    getComplianceSlope(),
                    getAngleLow(),
                    getAngleHigh(),
                    getSpeedLow(),
                    getSpeedHigh(),
                    (byte)(0xff*getTorque()),
                    (byte)(0x03*getTorque())
                }
                    );
        }

        private int calcLoadFromData(int data)
        {
            int sign = 1 - ((data & 1024) / 512);

            return sign * (data & 1023);

        }

        private double calcAngleFromData(int data)
        {
            double scale = 300.0 / 1024.0;

            return data * scale;
        }

        private byte getLED()
        {
            return 0;
        }

        private byte getComplianceMargin()
        {
            return 1;
        }

        private byte getComplianceSlope()
        {
            return 32;
        }

        private byte getTorque()
        {
            if (TorqueON == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        private byte getSpeedHigh()
        {
            double scale = 0.111;
            int value = (int)(SpeedLimit / scale);
            return (byte)((value >> 8) & 0xff);
        }


        private byte getSpeedLow()
        {
            double scale = 0.111;
            int value = (int)(SpeedLimit / scale);
            return (byte)(value & 0xff);
        }



        private byte getAngleHigh()
        {
            double scale = 1024.0 / 300.0;

            int value = (int)(this.TargetAngle * scale);

            return (byte)((value >> 8) & 0xff);
        }

        private byte getAngleLow()
        {
            double scale = 1024.0 / 300.0;

            int value = (int)(this.TargetAngle * scale);

            return (byte)(value & 0xff);
        }

    }
}
